<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\C_Buku;
use App\Http\Controllers\C_Anggota;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Home
Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [\App\Http\Controllers\LoginController::class, 'index'])->name('login');
Route::post('/login-auth', [\App\Http\Controllers\LoginController::class, 'login_auth'])->name('login-auth');



//Anggota
Route::get('/anggota',[\App\Http\Controllers\C_Anggota::class,'index']);

//Buku
Route::get('/buku',[\App\Http\Controllers\C_Buku::class,'index']);
Route::post('/buku', [C_Buku::class, 'store'])->name('buku.store');
Route::get('/buku/tambah', [\App\Http\Controllers\C_Buku::class, 'tambah'])->name('buku.tambah');
Route::get('/buku/{id}/edit', [\App\Http\Controllers\C_Buku::class, 'edit'])->name('buku.edit');
Route::put('/buku/{id}', [\App\Http\Controllers\C_Buku::class, 'update'])->name('buku.update');
Route::delete('/buku/{id}', [\App\Http\Controllers\C_Buku::class, 'destroy'])->name('buku.destroy');

//Route::post('/buku',[\App\Http\Controllers\C_Buku::class,'store']);
//Route::get('/buku/tambah',[\App\Http\Controllers\C_Buku::class,'tambah']);

