@extends('section.master')
@section('title')
Daftar Anggota
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Daftar Anggota</h6>
              <a href="{{ route('buku.tambah') }}" class="btn btn-primary btn-sm ms-auto float-end">Tambah Buku</a>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Alamat</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">E-mail</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nomor Telepon</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7" colspan="2">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($anggota as $anggota)
                    <tr>
                      <td>
                        <div class="d-flex px-2 py-1">
                          <div>
                            <img src="{{ asset('assets/img/team-2.jpg') }}" class="avatar avatar-sm me-3" alt="user1">
                          </div>
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">{{ $anggota->nama }}</h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <p class="text-xs text-secondary font-weight-bold mb-0">{{ $anggota->alamat }}</p>
                      </td>
                      <td class="align-middle text-center">
                        <span class="text-secondary text-xs font-weight-bold">{{ $anggota->email }}</span>
                      </td>
                      <td class="align-middle text-center">
                        <span class="text-secondary text-xs font-weight-bold">{{ $anggota->no_telp }}</span>
                      </td>
                      <td class="align-middle">
                      <button class="btn btn-warning btn-sm" onclick="location.href='{{ route('buku.edit', $anggota->id) }}'" data-toggle="tooltip" data-original-title="Edit user">
                        Edit
                      </button>
                      </td>
                      <td>
                      <form action="{{ route('buku.destroy', $anggota->id) }}" method="POST">
                          @method('DELETE')
                          @csrf
                          <input type="submit" value="Hapus" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus buku ini?')">
                      </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection