<?php

namespace App\Http\Controllers;

use App\Models\Anggota;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class C_Anggota extends Controller
{
    public function index()
    {
        $anggota = Anggota::all();
        // dd($anggota);
        return view('pages.anggota.anggota', compact('anggota'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'email' => 'required|email|unique:anggota,email',
            'password' => 'required|min:6',
            'alamat' => 'required',
            'no_telp' => 'required|numeric',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        $anggota = Anggota::create($validatedData);

        // Menghubungkan anggota dengan peran "anggota"
        $role = Role::where('name', 'anggota')->first();
        $anggota->role()->associate($role);
        $anggota->save();

        // Tindakan selanjutnya, misalnya mengirim email notifikasi atau mengarahkan pengguna ke halaman tertentu
        // ...

        return redirect()->back()->with('success', 'Anggota berhasil ditambahkan.');
    }
}
