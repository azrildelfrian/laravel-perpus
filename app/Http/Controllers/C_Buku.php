<?php

namespace App\Http\Controllers;

use App\Models\M_Buku;
use Illuminate\Http\Request;

class C_Buku extends Controller
{
    public function index()
    {
        $buku = M_Buku::all();
        //dd($buku);
        return view('pages.buku.buku',compact(['buku']));
    }

    public function tambah()
    {
        return view('pages.buku.tambah_buku');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $buku = new M_Buku();
        $buku->judul = $request->input('judul');
        $buku->penulis = $request->input('penulis');
        $buku->penerbit = $request->input('penerbit');
        $buku->tahun_terbit = $request->input('tahun_terbit');
        $buku->kategori = $request->input('kategori');
        $buku->jumlah_salinan = $request->input('jumlah_salinan');
        $buku->rak = $request->input('rak');
        $buku->sinopsis = $request->input('sinopsis');
        $buku->status = $request->input('status');
        $buku->save();

        // Redirect atau tampilkan pesan sukses jika perlu
        return redirect('/buku')->with('success', 'Data buku berhasil disimpan.');
    }

    public function edit($id)
    {
        $buku = M_Buku::findOrFail($id);
        return view('pages.buku.edit_buku', compact('buku'));
    }

    public function update(Request $request, $id)
    {
        $buku = M_Buku::findOrFail($id);
        $buku->judul = $request->input('judul');
        $buku->penulis = $request->input('penulis');
        $buku->penerbit = $request->input('penerbit');
        $buku->tahun_terbit = $request->input('tahun_terbit');
        $buku->kategori = $request->input('kategori');
        $buku->jumlah_salinan = $request->input('jumlah_salinan');
        $buku->rak = $request->input('rak');
        $buku->sinopsis = $request->input('sinopsis');
        $buku->status = $request->input('status');
        $buku->save();

        return redirect('/buku')->with('success', 'Data buku berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $buku = M_Buku::findOrFail($id);
        $buku->delete();

        return redirect('/buku')->with('success', 'Data buku berhasil dihapus.');
    }

    
}
